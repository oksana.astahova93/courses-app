import { useState } from 'react';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';

import { mockedCoursesList } from './constants';
import { AppProvider } from './components/Context';

function App() {
	const [showCreate, setShowCreate] = useState(false);

	const onShowCreate = () => {
		setShowCreate(true);
	};

	const onShowList = () => {
		setShowCreate(false);
	};

	return (
		<AppProvider>
			<Header />
			{showCreate ? (
				<CreateCourse onShowList={onShowList} />
			) : (
				<Courses list={mockedCoursesList} onShowCreate={onShowCreate} />
			)}
		</AppProvider>
	);
}
export default App;
