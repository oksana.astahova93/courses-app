import classNames from 'classnames';

import style from './button.module.scss';

const Button = (props) => {
	const { children, isDisabled, type = 'button', onClick, small } = props;
	return (
		<button
			type={type}
			disabled={isDisabled}
			onClick={onClick}
			className={classNames(style.button, { small })}
		>
			{children}
		</button>
	);
};

export default Button;
