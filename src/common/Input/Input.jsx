import classNames from 'classnames';

import style from './input.module.scss';

const Input = ({
	value,
	type = 'text',
	name,
	placeholder,
	isDisabled,
	onChange,
	className,
}) => {
	return (
		<>
			<input
				className={classNames(style.input, className)}
				value={value}
				name={name}
				type={type}
				disabled={isDisabled}
				placeholder={placeholder}
				onChange={onChange}
				id='search'
			/>
		</>
	);
};

export default Input;
