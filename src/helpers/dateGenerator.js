export const getDuration = (mins) => {
	const hours = Math.floor(mins / 60);
	const minutes = mins - hours * 60;
	return `${hours < 10 ? '0' : ''}${hours}:${
		minutes < 10 ? '0' : ''
	}${minutes}`;
};
