import { useState, useContext } from 'react';
import { AppContext } from '../Context';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';

import { getDuration } from '../../helpers/dateGenerator';

import style from './createCourse.module.scss';

const CreateCourse = ({ onShowList }) => {
	const [course, setCourse] = useState({
		title: '',
		description: '',
		duration: '',
		authors: [],
	});

	const [newAuthor, setNewAuthor] = useState({ name: '' });

	const { addCourse, addAuthor, authors } = useContext(AppContext);

	const setCourseHandler = (e) => {
		setCourse({
			...course,
			[e.target.name]: e.target.value,
		});
	};

	const handleAddCourse = () => {
		if (
			!course.title ||
			!course.description ||
			!course.authors.length ||
			!course.duration
		) {
			alert('Please, fill in all fields');
			return;
		}
		addCourse(course);
		onShowList();
	};

	const setAuthorHandler = (e) => {
		setNewAuthor({
			...newAuthor,
			name: e.target.value,
		});
	};

	const handleAddAuthor = () => {
		addAuthor(newAuthor);
		setNewAuthor({ name: '' });
	};

	console.log(course);

	return (
		<div className={style.container}>
			<form className={style.header}>
				<label htmlFor='course-title'>Title</label>
				<div className={style.row}>
					<Input
						name='title'
						value={course.title}
						placeholder='Enter title...'
						id='course-title'
						onChange={setCourseHandler}
					/>
					<Button onClick={handleAddCourse}>Create course</Button>
				</div>

				<label htmlFor='textarea'>Description</label>
				<textarea
					name='description'
					value={course.description}
					className={style.description}
					id='textarea'
					rows='8'
					placeholder='Enter description...'
					minLength='2'
					onChange={setCourseHandler}
				></textarea>
			</form>
			<div className={style.info}>
				<div className={style.col}>
					<div className={style.addAuthor}>
						<h2>Add author</h2>
						<label htmlFor='author-name'>Author name</label>
						<Input
							placeholder='Enter author name...'
							id='author-name'
							value={newAuthor.name}
							name='author'
							onChange={setAuthorHandler}
						/>
						<Button
							onClick={handleAddAuthor}
							className={style.button}
						>
							Create author
						</Button>
					</div>
					<div className={style.duration}>
						<h2>Duration</h2>
						<label htmlFor='duration'>Duration</label>
						<Input
							placeholder='Enter duration in minutes...'
							id='duration'
							name='duration'
							onChange={setCourseHandler}
							type='number'
						/>
						{course.duration !== undefined && (
							<span>
								Duration: {getDuration(course.duration)} hours
							</span>
						)}
					</div>
				</div>
				<div className={style.col}>
					<div className={style.authorsList}>
						<h2>Authors</h2>
						<ul>
							{authors?.map((author) => (
								<li key={author.id}>
									{author.name}
									<Button
										onClick={() => {
											if (
												!course.authors.includes(
													author.id
												)
											) {
												setCourse({
													...course,
													authors: [
														...course.authors,
														author.id,
													],
												});
											}
										}}
									>
										Add author
									</Button>
								</li>
							))}
						</ul>
						<h2>Course authors</h2>
						{course.authors.length ? (
							<ul>
								{course.authors?.map((authorId) => (
									<li key={`${authorId} course`}>
										{
											authors.find(
												({ id }) => id === authorId
											).name
										}
										<Button
											onClick={() => {
												setCourse({
													...course,
													authors:
														course.authors.filter(
															(id) =>
																id !== authorId
														),
												});
											}}
										>
											&times;
										</Button>
									</li>
								))}
							</ul>
						) : (
							<span>Author list is empty</span>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
