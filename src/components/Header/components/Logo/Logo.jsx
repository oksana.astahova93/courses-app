import style from './logo.module.scss';

import logo from '../../../../img/EPAM_logo.png';

const Logo = () => {
	return (
		<div className={style.logo}>
			<img src={logo} alt='logo of EPAM' />
		</div>
	);
};

export default Logo;
