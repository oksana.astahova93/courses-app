import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import style from './header.module.scss';

function Header() {
	return (
		<div className={style.container}>
			<Logo />
			<div>
				<span>Oksana</span>
				<Button small>Logout</Button>
			</div>
		</div>
	);
}
export default Header;
