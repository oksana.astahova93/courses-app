import Button from '../../../../common/Button/Button';

import { getDuration } from '../../../../helpers/dateGenerator';

import style from './courseCard.module.scss';

const CourseCard = ({ item, authors }) => {
	return (
		<li className={style.courses__item}>
			<div className={style.description}>
				<h1>{item.title}</h1>
				<p>{item.description}</p>
			</div>
			<div className={style.information}>
				<span>
					<strong>Authors:</strong>
				</span>
				<span className={style.authors}>
					{item.authors
						.map(
							(author) =>
								authors.find((a) => a.id === author)?.name
						)
						.join(', ')}
				</span>
				<div className={style.duration}>
					<span>
						<strong>Duration:</strong>
					</span>
					{getDuration(item.duration)} hours
				</div>
				<div>
					<span>
						<strong>Created:</strong>
					</span>
					{new Intl.DateTimeFormat({}).format(
						new Date(
							item.creationDate.split('/')[2],
							item.creationDate.split('/')[1] - 1,
							item.creationDate.split('/')[0]
						)
					)}
				</div>
				<div className={style.button}>
					<Button>Show course</Button>
				</div>
			</div>
		</li>
	);
};

export default CourseCard;
