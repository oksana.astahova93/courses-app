import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';

import style from './searchBar.module.scss';

const SearchBar = ({ search, onSearchChange, onSearch, onClear }) => {
	return (
		<form onSubmit={onSearch} className={style.form}>
			<div className={style.wrapper}>
				<Input
					placeholder='Enter course name...'
					value={search}
					onChange={onSearchChange}
					className={style.input}
				/>
				{search && (
					<button
						className={style.clear}
						onClick={onClear}
						type='button'
					>
						&times;
					</button>
				)}
			</div>
			<Button type='submit'>Search</Button>
		</form>
	);
};

export default SearchBar;
