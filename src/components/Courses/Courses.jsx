import { useState, useContext } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';

import { mockedAuthorsList } from '../../constants';

import { AppContext } from '../Context';

import style from './courses.module.scss';

const Courses = ({ onShowCreate }) => {
	const [search, setSearch] = useState('');

	const { searchCourses, courses } = useContext(AppContext);

	const onSearchChange = (e) => {
		setSearch(e.target.value);
	};

	const handleSearch = (e) => {
		e.preventDefault();
		searchCourses(search);
		// console.log('submit');
	};

	const handleClear = () => {
		setSearch('');
		searchCourses('');
	};

	return (
		<div className={style.container}>
			<div className={style.search__wrapper}>
				<SearchBar
					search={search}
					onSearchChange={onSearchChange}
					onSearch={handleSearch}
					onClear={handleClear}
				/>
				<Button onClick={onShowCreate}>Add new course</Button>
			</div>
			<ul className={style.courses}>
				{courses?.map((item) => (
					<CourseCard
						item={item}
						key={item.id}
						authors={mockedAuthorsList}
					/>
				))}
			</ul>
		</div>
	);
};

export default Courses;
