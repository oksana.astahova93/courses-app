import { createContext, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { mockedCoursesList, mockedAuthorsList } from '../constants';

export const AppContext = createContext();

export const AppProvider = ({ children }) => {
	const [originalCourses, setOriginalCourses] = useState(mockedCoursesList);

	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);

	const searchCourses = (search) => {
		setCourses(
			originalCourses.filter((course) =>
				course.title.toLowerCase().includes(search.toLowerCase())
			)
		);
		console.log(searchCourses, search);
	};

	const addCourse = (course) => {
		const date = new Date();
		const newCourse = {
			...course,
			id: uuidv4(),
			creationDate: `${date.getDate()}/${
				date.getMonth() + 1
			}/${date.getFullYear()}`,
		};
		console.log(newCourse);
		const newCourses = [...originalCourses, newCourse];
		setOriginalCourses(newCourses);
		setCourses(newCourses);
	};

	const addAuthor = (author) => {
		if (!author.name) {
			return;
		}
		const newAuthor = {
			...author,
			id: uuidv4(),
		};
		const newAuthors = [...authors, newAuthor];
		setAuthors(newAuthors);
	};

	return (
		<AppContext.Provider
			value={{
				courses,
				authors,
				searchCourses,
				addCourse,
				addAuthor,
			}}
		>
			{children}
		</AppContext.Provider>
	);
};
